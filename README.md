Simple audio processor implemented using LabVIEW 2018 and the Matchscript node expansion. 

Credit for the OLA VIs and overall direction of this project to Dr. Ed Doering
of Rose-Hulman Institute of Technology. 